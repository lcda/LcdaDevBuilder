FROM tianon/wine:32

USER root
RUN apt update \
 && apt install -y git curl unzip \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


ENV WINEPREFIX /root/.wine
ENV WINEPATH c:\\windows;c:\\windows\\system;c:\\NWNScriptCompiler

RUN wine wineboot --init \
	&& curl -SL https://github.com/CromFr/STNeverwinterScript/releases/download/NWNScriptCompiler140705/NWNScriptCompiler.zip > /tmp/NWNScriptCompiler.zip \
	&& unzip /tmp/NWNScriptCompiler.zip -d $WINEPREFIX/drive_c/ \
	&& ln -s $WINEPREFIX/drive_c/NWNScriptCompiler ~ \
	&& rm /tmp/NWNScriptCompiler.zip


CMD bash
